component{
	this.name = "Twitter API Extension test suite";

	this.sessionManagement = true;

	this.mappings[ "/tests"   ] = ExpandPath( "/" );
	this.mappings[ "/twitter" ] = ExpandPath( "../" );

	public boolean function onRequestStart( String targetPage ){
		return true;
	}
}