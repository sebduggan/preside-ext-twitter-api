<cfscript>
	expandedUrl = args.expanded_url ?: "";
	displayUrl  = rereplacenocase( args.display_url ?: "", "([^a-z0-9])", "\1&##8203;", "all" );
</cfscript>
<cfoutput>
	<a href="#expandedUrl#" rel="noopener noreferrer" target="_blank">#displayUrl#</a>
</cfoutput>