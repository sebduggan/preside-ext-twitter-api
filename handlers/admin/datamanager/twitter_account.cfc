component {

	property name="twitterApiWrapper"    inject="TwitterApiWrapper";
	property name="presideObjectService" inject="PresideObjectService";

	private void function postAddRecordAction( event, rc, prc, args={} ) {
		var newId = args.newId ?: "";

		if ( len( args.formData.access_token ?: "" ) && len( args.formData.access_token_secret ?: "" ) ) {
			var userInfo = twitterApiWrapper.verifyCredentials( account=newId );
			presideObjectService.updateData( objectName="twitter_account", id=newId, data={ screen_name=userInfo.getResponse().screen_name ?: "" } );
			return;
		}

		setNextEvent( url=event.buildAdminLink( linkTo="TwitterAuth.requestToken", queryString="accountId=#newId#" ) );
	}

	private string function preRenderRecord( event, rc, prc, args={} ) {
		var verifyCredentials = twitterApiWrapper.verifyCredentials( account=prc.recordId );

		if ( verifyCredentials.getSuccess() ) {
			return "";
		}

		return renderView( view="/admin/twitter/invalidCredentials", args=args );
	}

	private void function extraTopRightButtonsForViewRecord( event, rc, prc, args={} ) {
		var objectName = args.objectName ?: "";
		var recordId   = prc.recordId    ?: "";

		args.actions = args.actions ?: [];
		args.actions.prepend({
			  link      = event.buildAdminLink( linkTo="TwitterAuth.requestToken", querystring="accountId=#recordId#" )
			, btnClass  = "btn-info"
			, iconClass = "fa-handshake-o"
			, title     = translateResource( "twitter:oauth.refresh.credentials" )
		} );
	}
}
