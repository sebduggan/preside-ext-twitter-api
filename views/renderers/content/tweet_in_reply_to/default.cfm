<cfscript>
	inReplyTo           = args.inReplyTo           ?: [];
	inreplyToStatus     = args.inreplyToStatus     ?: "";
	inreplyToScreenName = args.inreplyToScreenName ?: "";
	replyingTo          = "";

	inReplyTo.each( function( item, index, array ) {
		if ( index > 1 ) {
			if ( index == inReplyTo.len() ) {
				replyingTo &= " and ";
			} else {
				replyingTo &= ", ";
			}
		}
		replyingTo &= item;
	} );
</cfscript>
<cfoutput>
	<div class="tweet-in-reply-to">
		<a href="https://twitter.com/#inreplyToScreenName#/status/#inreplyToStatus#" class="tweet-date" rel="noopener noreferrer" target="_blank" title="#translateResource( uri="twitter:view.replied.to.tweet.label" )#"><svg role="presentation" aria-hidden="true"><use xlink:href="##tweet-action-reply" /></svg></a>
		#translateResource( uri="twitter:replying.to.label" )# #replyingTo#
	</div>
</cfoutput>