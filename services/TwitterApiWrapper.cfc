/**
 * @presideService
 * @singleton
 */
component {
// CONSTRUCTOR
	/**
	 * @utils.inject TwitterUtils
	 */
	public any function init( required any utils ) {
		_setUtils( arguments.utils );
		return this;
	}

//PUBLIC METHODS
	/**
	 * Makes a call to the statuses/user_timeline endpoint:
	 * https://developer.twitter.com/en/docs/tweets/timelines/api-reference/get-statuses-user_timeline
	 *
	 * Returns a TwitterRequest object which will contain the success status and the returned data.
	 *
	 * @account  The account to use for the request - either a Preside record ID or a struct/query record containing the account config
	 * @params   The parameters to pass to the API endpoint, as defined in the API reference above
	 */
	public TwitterRequest function getUserTimeline( required any account, struct params={} ) {
		return _getUtils().newRequest(
			  argumentCollection = arguments
			, method             = "GET"
			, apiVersion         = "1.1"
			, endpoint           = "statuses/user_timeline.json"
		);
	}

	/**
	 * Makes a call to the statuses/lookup endpoint:
	 * https://developer.twitter.com/en/docs/tweets/post-and-engage/api-reference/get-statuses-lookup
	 *
	 * Returns a TwitterRequest object which will contain the success status and the returned data.
	 *
	 * @account  The account to use for the request - either a Preside record ID or a struct/query record containing the account config
	 * @params   The parameters to pass to the API endpoint, as defined in the API reference above
	 */
	public TwitterRequest function getStatuses( required any account, struct params={} ) {
		return _getUtils().newRequest(
			  argumentCollection = arguments
			, method             = "GET"
			, apiVersion         = "1.1"
			, endpoint           = "statuses/lookup.json"
		);
	}

	/**
	 * Makes a call to the statuses/update endpoint:
	 * https://developer.twitter.com/en/docs/tweets/post-and-engage/api-reference/post-statuses-update
	 *
	 * Posts a new status update (Tweet).
	 *
	 * @account  The account to use for the request - either a Preside record ID or a struct/query record containing the account config
	 * @params   The parameters to pass to the API endpoint, as defined in the API reference above
	 */
	public TwitterRequest function postStatus( required any account, struct params={} ) {
		return _getUtils().newRequest(
			  argumentCollection = arguments
			, method             = "POST"
			, apiVersion         = "1.1"
			, endpoint           = "statuses/update.json"
		);
	}

	/**
	 * Makes a call to the account/verify_credentials endpoint:
	 * https://developer.twitter.com/en/docs/twitter-api/v1/accounts-and-users/manage-account-settings/api-reference/get-account-verify_credentials
	 *
	 * Verifies an account's credentials are valid and returns the user object if they are
	 *
	 * @account  The account to use for the request - either a Preside record ID or a struct/query record containing the account config
	 * @params   The parameters to pass to the API endpoint, as defined in the API reference above
	 */
	public TwitterRequest function verifyCredentials( required any account, struct params={} ) {
		return _getUtils().newRequest(
			  argumentCollection = arguments
			, method             = "GET"
			, apiVersion         = "1.1"
			, endpoint           = "account/verify_credentials.json"
		);
	}

	/**
	 * Makes a call to the oauth/request_token endpoint:
	 * https://developer.twitter.com/en/docs/authentication/api-reference/request_token
	 *
	 * Returns a TwitterRequest object which will contain the success status and the returned data.
	 *
	 * @accountId  The accountId to use for the request
	 */
	public TwitterRequest function getRequestToken( required string accountId ) {
		var params = {
			oauth_callback = $getRequestContext().buildAdminLink( linkTo="TwitterAuth.callback", querystring="accountId=#arguments.accountId#" )
		};

		return _getUtils().newRequest(
			  account          = arguments.accountId
			, params           = params
			, method           = "POST"
			, endpoint         = "oauth/request_token"
			, expectedResponse = "querystring"
			, useAccessToken   = false
		);
	}

	/**
	 * Makes a call to the oauth/access_token endpoint:
	 * https://developer.twitter.com/en/docs/authentication/api-reference/access_token
	 *
	 * Returns a TwitterRequest object which will contain the success status and the returned data.
	 *
	 * @accountId      The accountId to use for the request
	 * @oAuthToken     The oauth_token returned in the callback from Twitter
	 * @oAuthVerifier  The oauth_verifier returned in the callback from Twitter
	 */
	public TwitterRequest function getAccessToken( required string accountId, required string oAuthToken, required string oAuthVerifier ) {
		var params = {
			  oauth_token    = arguments.oAuthToken
			, oauth_verifier = arguments.oAuthVerifier
		};

		var accessToken = _getUtils().newRequest(
			  account          = arguments.accountId
			, params           = params
			, method           = "POST"
			, endpoint         = "oauth/access_token"
			, expectedResponse = "querystring"
			, useAccessToken   = false
		);

		if ( $helpers.isTrue( accessToken.getSuccess() ?: "" ) ) {
			var dao      = $getPresideObject( "twitter_account" );
			var data     = accessToken.getResponse();
			var existing = dao.selectData( id=arguments.accountId );

			if ( len( existing.user_id ) && existing.user_id != data.user_id ) {
				accessToken.setSuccess( false );
				accessToken.setResponse( $translateResource( "twitter:oauth.link.user.mismatch" ) );
				return accessToken;
			}

			var duplicateScreenName = dao.dataExists(
				  filter       = "id != :id and ( screen_name = :screen_name or user_id = :user_id )"
				, filterParams = {
					  id          = arguments.accountId
					, screen_name = data.screen_name
					, user_id     = data.user_id
				}
			);
			if ( duplicateScreenName ) {
				accessToken.setSuccess( false );
				accessToken.setResponse( $translateResource( "twitter:duplicate.account" ) );
				return accessToken;
			}

			dao.updateData(
				  id   = arguments.accountId
				, data = {
					  screen_name         = data.screen_name
					, user_id             = data.user_id
					, access_token        = data.oauth_token
					, access_token_secret = data.oauth_token_secret
				}
			);
		}

		return accessToken;
	}

 // GETTERS AND SETTERS
	private any function _getUtils() {
		return _utils;
	}
	private void function _setUtils( required any utils ) {
		_utils = arguments.Utils;
	}

}