<cfscript>
	uuid           = args.itemData.uuid      ?: "";
	mediaUrl       = args.itemData.url       ?: "";
	mediaExtension = args.itemData.extension ?: "";
	mediaBaseUrl   = args.itemData.baseUrl   ?: "";
	altText        = args.ext_alt_text       ?: "";
	width          = args.sizes.large.w      ?: 1280;
	height         = args.sizes.large.h      ?: 720;
	embeddable     = isTrue( args.additional_media_info.embeddable ?: true );
	videoUrl       = embeddable ? ( args.video_info.variants[ 2 ].url ?: "" ) : ( args.expanded_url ?: "" );
	image          = "#mediaBaseUrl#?format=#mediaExtension#&name=small";
	playVideoLabel = translateResource( uri="twitter:play.video.label" );
	uncaptioned    = translateResource( uri="twitter:uncaptioned.video.label" );
</cfscript>
<cfoutput>
	<a href="#videoUrl#"
		<cfif embeddable>
		data-mediabox="tweet-gallery-#uuid#"
		data-iframe="true"
		data-width="#width#"
		data-height="#height#"
		</cfif>
		class="tweet-media-item"
		rel="noopener noreferrer"
		aria-label="#playVideoLabel#"
		target="_blank">
		<img src="#image#" alt="#len( altText ) ? altText : uncaptioned#">
		<div class="tweet-media-video-play">
			<svg role="img" aria-label="#playVideoLabel#"><use xlink:href="##tweet-media-video-play" /></svg>
		</div>
	</a>
</cfoutput>
