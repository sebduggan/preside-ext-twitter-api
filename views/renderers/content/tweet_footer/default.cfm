<cfscript>
	isRetweet    = !isEmpty( args.tweetData.retweeted_status ?: {} );
	tweetData    = isRetweet ? args.tweetData.retweeted_status : args.tweetData;
	replyCount   = args.replyCount   ?: 0;
	retweetCount = args.retweetCount ?: 0;
	likeCount    = args.likeCount    ?: 0;

	likeLabel    = translateResource( uri="twitter:like.label" );
	likesLabel   = translateResource( uri="twitter:likes.label" );
	replyLabel   = translateResource( uri="twitter:reply.label" );
	repliesLabel  = translateResource( uri="twitter:replies.label" );
	retweetLabel  = translateResource( uri="twitter:retweet.label" );
	retweetsLabel = translateResource( uri="twitter:retweets.label" );
</cfscript>
<cfoutput>
	<div class="tweet-footer">
		<a href="https://twitter.com/intent/tweet?in_reply_to=#tweetData.id_str#" class="tweet-action tweet-action-reply" rel="noopener noreferrer" target="_blank" title="#replyLabel#"><svg role="img" aria-label="#replyLabel#"><use xlink:href="##tweet-action-reply" /></svg><cfif replyCount> <span class="tweet-action-count" aria-label="#replyCount# replies">#replyCount#</span></cfif></a>

		<a href="https://twitter.com/intent/retweet?tweet_id=#tweetData.id_str#" class="tweet-action tweet-action-retweet" rel="noopener noreferrer" target="_blank" title="#retweetLabel#"><svg role="img" aria-label="#retweetLabel#"><use xlink:href="##tweet-action-retweet" /></svg><cfif retweetCount> <span class="tweet-action-count" aria-label="#retweetCount# retweets">#retweetCount#</span></cfif></a>

		<a href="https://twitter.com/intent/like?tweet_id=#tweetData.id_str#" class="tweet-action tweet-action-like" rel="noopener noreferrer" target="_blank" title="#likeLabel#"><svg role="img" aria-label="#likeLabel#"><use xlink:href="##tweet-action-like" /></svg><cfif likeCount> <span class="tweet-action-count" aria-label="#likeCount# likes">#likeCount#</span></cfif></a>
	</div>
</cfoutput>
