/**
 * @versioned                       false
 * @dataManagerGroup                Twitter
 * @dataManagerDisallowedOperations clone
 */

component {
	property name="consumer_api_key"        type="string"  dbtype="varchar" maxlength=100 required=true;
	property name="consumer_api_secret_key" type="string"  dbtype="varchar" maxlength=100 required=true;
}