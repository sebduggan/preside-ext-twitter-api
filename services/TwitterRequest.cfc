component accessors="true" {

	property name="utils"                     type="any";
	property name="method"                    type="string"  default="GET";
	property name="endpoint"                  type="string"  default="";
	property name="consumerApiKey"            type="string"  default="";
	property name="consumerApiSecretKey"      type="string"  default="";
	property name="consumerAccessToken"       type="string"  default="";
	property name="consumerAccessTokenSecret" type="string"  default="";
	property name="params"                    type="struct"  default=structNew();
	property name="authHeader"                type="string"  default="";
	property name="success"                   type="boolean" default=false;
	property name="rawResponse"               type="any";
	property name="response"                  type="any";
	property name="expectedResponse"          type="string"  default="json";

	public twitterRequest function buildAuthHeader() {
		getUtils().buildAuthHeader( this );
		return this;
	}

	public twitterRequest function submit() {
		getUtils().submit( this );
		return this;
	}

	public string function getSigningKey() {
		return getUtils().percentEncode( getConsumerApiSecretKey() ) & "&" & getUtils().percentEncode( getConsumerAccessTokenSecret() );
	}

}