/**
 * @presideService
 * @singleton
 */
component {
// CONSTRUCTOR
	public any function init() {
		_setBaseUrl( "https://api.twitter.com/" );

		return this;
	}

//PUBLIC METHODS
	/**
	 * Wrapper method to create and submit a request to the Twitter API.
	 *
	 * Returns a TwitterRequest object, which will include success status and the response data.
	 *
	 * @method    The HTTP method to use for the request
	 * @endpoint  The API endpoint for the request - what comes after https://api.twitter.com/1.1/
	 * @account   The twitter_account to use for the request. Either a Preside record ID, or a struct or query record containing the data for that account
	 * @params    A struct of parameters to be passed to the endpoint. These should be named and typed according to what the Twitter endpoint is expecting
	 */
	public TwitterRequest function newRequest(
		  required string  method
		, required string  endpoint
		, required any     account
		,          string  apiVersion       = ""
		,          string  expectedResponse = "json"
		,          boolean useAccessToken   = true
		,          struct  params           = {}
	) {
		var accountConfig  = _getAccountConfig( arguments.account );
		var twitterRequest = new TwitterRequest(
			  utils                     = this
			, method                    = arguments.method
			, endpoint                  = _getBaseUrl() & listAppend( arguments.apiVersion, arguments.endpoint, "/" )
			, expectedResponse          = arguments.expectedResponse
			, params                    = arguments.params
			, consumerApiKey            = accountConfig.consumer_api_key        ?: ""
			, consumerApiSecretKey      = accountConfig.consumer_api_secret_key ?: ""
			, consumerAccessToken       = arguments.useAccessToken ? ( accountConfig.access_token        ?: "" ) : ""
			, consumerAccessTokenSecret = arguments.useAccessToken ? ( accountConfig.access_token_secret ?: "" ) : ""
		);
		twitterRequest.buildAuthHeader().submit();

		return twitterRequest;
	}

	/**
	 * Submits a configured TwitterRequest object to the Twitter API.
	 *
	 * Results and success status are added into the supplied TwitterRequest.
	 * If successful, twitterRequest.getResponse() will contain the returned data.
	 * If the call fails, twitterRequest.getResponse() will contain an error desciption.
	 *
	 * @twitterRequest  A TwitterRequest object, configured with details of the request to be made
	 */
	public void function submit( required TwitterRequest twitterRequest ) {
		var response  = "";
		var params    = arguments.twitterRequest.getParams();
		var method    = arguments.twitterRequest.getMethod();

		try {
			http url=arguments.twitterRequest.getEndpoint()
				method=method
				result="response"
				timeout="30"
			{
				httpparam type="header" name="Authorization" value=arguments.twitterRequest.getAuthHeader();

				for( var key in params ) {
					httpparam type="url" name=key value=percentEncode( params[ key ] );
				}
			}
			arguments.twitterRequest.setRawResponse( response );

			if ( response.status_code == 200 ) {
				arguments.twitterRequest.setSuccess( true );
				if ( arguments.twitterRequest.getExpectedResponse() == "json" ) {
					arguments.twitterRequest.setResponse( deserializeJson( response.filecontent ) );
				} else if ( arguments.twitterRequest.getExpectedResponse() == "querystring" ) {
					arguments.twitterRequest.setResponse( _parseQueryString( response.filecontent ) );
				} else {
					arguments.twitterRequest.setResponse( response.filecontent );
				}
			} else {
				var messages    = [];
				var filecontent = "";
				messages.append( response.statuscode ?: "" );
				if ( isJson( response.filecontent ?: "" ) ) {
					filecontent = deserializeJson( response.filecontent ?: "" );
					for( var error in filecontent.errors ?: [] ) {
						messages.append( error.message );
					}
				}
				arguments.twitterRequest.setResponse( messages.toList( " | " ) );

				throw( type="twitter.api.error", message=response.statuscode ?: "Unknown error", detail=serializeJson( response ) );
			}
		}
		catch( any e ) {
			$raiseError( e );
		}
	}

	/**
	 * Generates a signature and adds an OAuth authorisation header to the supplied
	 * TwitterRequest object.
	 *
	 * @twitterRequest  A TwitterRequest object, configured with details of the request to be made
	 */
	public void function buildAuthHeader( required TwitterRequest twitterRequest ) {
		var authValues = {
			  "oauth_consumer_key"     = arguments.twitterRequest.getConsumerApiKey()
			, "oauth_nonce"            = _generateNonce()
			, "oauth_signature_method" = "HMAC-SHA1"
			, "oauth_timestamp"        = _getTimestamp()
			, "oauth_token"            = arguments.twitterRequest.getConsumerAccessToken()
			, "oauth_version"          = "1.0"
		};
		var baseSignature = generateBaseSignature(
			  method         = arguments.twitterRequest.getMethod()
			, endpoint       = arguments.twitterRequest.getEndpoint()
			, authValues     = authValues
			, params         = arguments.twitterRequest.getParams()
		);
		authValues[ "oauth_signature" ] = encodeSignature( arguments.twitterRequest, baseSignature );

		var authString = encodeAndConcat( values=authValues, separator=", ", qualifier='"' );

		arguments.twitterRequest.setAuthHeader( "OAuth " & authString );
	}

	/**
	 * Sorts a struct of values, percent-encodes them, and concatenates them into a single string.
	 *
	 * @values     A struct containing the key-values to be encoded/concatenated
	 * @separator  A string used to join the encoded items into a single list
	 * @qualifier  A optional string used to wrap around either end of the encoded values
	 */
	public string function encodeAndConcat( required struct values, required string separator, string qualifier="" ) {
		var parts = [];

		for( var key in arguments.values.keyArray().sort( "text" ) ) {
			parts.append( '#percentEncode( key )#=#arguments.qualifier##percentEncode( arguments.values[ key ] )##arguments.qualifier#' );
		}

		return parts.toList( arguments.separator );
	}

	/**
	 * Generates a base signature for use in the OAuth authorisation header
	 *
	 * @method      The HTTP method to use for the request
	 * @endpoint    The API endpoint for the request
	 * @authValues  A struct of authorisation keys and tokens
	 * @params      A struct of parameters to be passed to the endpoint
	 */
	public string function generateBaseSignature(
		  required string method
		, required string endpoint
		, required struct authValues
		, required struct params
	) {
		var parts     = [];
		var allParams = {};
		allParams.append( arguments.authValues );
		allParams.append( arguments.params );

		parts.append( uCase( arguments.method ) );
		parts.append( percentEncode( arguments.endpoint ) );
		parts.append( percentEncode( encodeAndConcat( values=allParams, separator="&" ) ) );

		return parts.toList( "&" );
	}

	/**
	 * Encodes a signature for use in the OAuth authorisation header using HMAC-SHA1 and Base64
	 *
	 * @twitterRequest  The TwitterRequest configured with
	 * @baseSignature   The generated signature for the request
	 */
	public string function encodeSignature( required TwitterRequest twitterRequest, required string baseSignature ) {
		var encoded = hmac( arguments.baseSignature, arguments.twitterRequest.getSigningKey(), "HmacSHA1" );

		return toBase64( binaryDecode( encoded, "hex" ) );
	}

	/**
	 * Takes the supplied string and percent-encodes it according to Twitter's specification:
	 * https://developer.twitter.com/en/docs/basics/authentication/oauth-1-0a/percent-encoding-parameters
	 *
	 * @input  A string to be encoded
	 */
	public string function percentEncode( required string input ) {
		var inputString = javaCast( "string", arguments.input );
		var codepoints  = inputString.codepoints().toArray();
		var output      = [];
		var validChars  = [
			  "0", "1", "2", "3", "4", "5", "6", "7", "8", "9"
			, "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"
			, "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"
			, "-", ".", "_", "~"
		];

		for( var codepoint in codepoints ) {
			if ( codepoint <=126 && validChars.find( chr( codepoint ) ) ) {
				output.append( chr( codepoint ) );
			} else {
				output.append( urlEncodedFormat( _chrFromCodepoint( codepoint ) ) )
			}
		}

		return output.toList( "" );
	}

// PRIVATE METHODS
	private string function _generateNonce() {
		var minInt = 0;
		var maxInt = createObject("java","java.lang.Integer").MAX_VALUE;
		var nonce  = _getTimestamp() & randRange( minInt, maxInt );

		return hash( nonce, "SHA" );
	}

	private numeric function _getTimestamp() {
		return int( getTickcount() / 1000 );
	}

	private any function _getAccountConfig( required any account ) {
		if ( isSimpleValue( arguments.account ) ) {
			return $getPresideObject( "twitter_account" ).selectData(
				  id                = arguments.account
				, extraSelectFields = [ "developer_app.consumer_api_key", "developer_app.consumer_api_secret_key" ]
			);
		}

		return arguments.account;
	}

	private string function _chrFromCodepoint( required numeric codepoint ) {
		if ( codepoint <= 65535 ) {
			return( chr( codepoint ) );
		}

		var chars = createObject( "java", "java.lang.Character" ).toChars( javaCast( "int", codepoint ) );

		return( arrayToList( chars, "" ) );
	}

	private struct function _parseQueryString( required string qs ) {
		var parsed = {};

		for( var pair in listToArray( arguments.qs, "&" ) ) {
			parsed[ listFirst( pair, "=" ) ] = listRest( pair, "=" );
		}

		return parsed;
	}

 // GETTERS AND SETTERS
	private any function _getBaseUrl() {
		return _baseUrl;
	}
	private void function _setBaseUrl( required any baseUrl ) {
		_baseUrl = arguments.baseUrl;
	}

}