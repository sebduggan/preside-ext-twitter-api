component extends="testbox.system.BaseSpec" {

	function run(){

		describe( "percentEncode", function(){

			it( "should percent-encode strings to Twitter's specification", function(){
				var utils = _getUtils();

				expect( utils.percentEncode( "" ) ).toBe( "" );
				expect( utils.percentEncode( "Ladies + Gentlemen" ) ).toBe( "Ladies%20%2B%20Gentlemen" );
				expect( utils.percentEncode( "An encoded string!" ) ).toBe( "An%20encoded%20string%21" );
				expect( utils.percentEncode( "Dogs, Cats & Mice" ) ).toBe( "Dogs%2C%20Cats%20%26%20Mice" );
				expect( utils.percentEncode( "☃" ) ).toBe( "%E2%98%83" );
				expect( utils.percentEncode( "🏏" ) ).toBe( "%F0%9F%8F%8F" );
			} );

		});

		describe( "generateBaseSignature", function(){

			it( "should create an unencoded signature string to Twitter's specification", function(){
				var utils = _getUtils();

				var method     = "POST";
				var baseUrl    = "https://api.twitter.com/1.1/statuses/update.json";
				var authValues = {
					  "oauth_consumer_key"     = "xvz1evFS4wEEPTGEFPHBog"
					, "oauth_nonce"            = "kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg"
					, "oauth_signature_method" = "HMAC-SHA1"
					, "oauth_timestamp"        = "1318622958"
					, "oauth_token"            = "370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb"
					, "oauth_version"          = "1.0"
				};
				var params     = {
					  "status"           = "Hello Ladies + Gentlemen, a signed OAuth request!"
					, "include_entities" = true
				};

				var expectedResult = "POST&https%3A%2F%2Fapi.twitter.com%2F1.1%2Fstatuses%2Fupdate.json&include_entities%3Dtrue%26oauth_consumer_key%3Dxvz1evFS4wEEPTGEFPHBog%26oauth_nonce%3DkYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1318622958%26oauth_token%3D370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb%26oauth_version%3D1.0%26status%3DHello%2520Ladies%2520%252B%2520Gentlemen%252C%2520a%2520signed%2520OAuth%2520request%2521";

				expect( utils.generateBaseSignature( method=method, endpoint=baseUrl, authValues=authValues, params=params ) ).toBe( expectedResult );
			} );

		});

		describe( "encodeSignature", function(){

			it( "should correctly encode a signature string using HMAC-SHA1", function(){
				var utils          = _getUtils();
				var twitterRequest = _newTwitterRequest();

				twitterRequest.setConsumerApiSecretKey( "kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw" );
				twitterRequest.setConsumerAccessTokenSecret( "LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE" );

				var baseSignature  = "POST&https%3A%2F%2Fapi.twitter.com%2F1.1%2Fstatuses%2Fupdate.json&include_entities%3Dtrue%26oauth_consumer_key%3Dxvz1evFS4wEEPTGEFPHBog%26oauth_nonce%3DkYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg%26oauth_signature_method%3DHMAC-SHA1%26oauth_timestamp%3D1318622958%26oauth_token%3D370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb%26oauth_version%3D1.0%26status%3DHello%2520Ladies%2520%252B%2520Gentlemen%252C%2520a%2520signed%2520OAuth%2520request%2521";
				var expectedResult = "hCtSmYh+iHYCEqBWrE7C7hYmtUk=";

				expect( utils.encodeSignature( twitterRequest=twitterRequest, baseSignature=baseSignature ) ).toBe( expectedResult );
			} );

		});

		describe( "buildAuthHeader", function(){

			it( "should create a signed OAuth authorzation header", function(){
				var utils          = _getUtils();
				var twitterRequest = _newTwitterRequest();

				twitterRequest.setMethod( "POST" );
				twitterRequest.setEndpoint( "https://api.twitter.com/1.1/statuses/update.json" );
				twitterRequest.setConsumerApiKey( "xvz1evFS4wEEPTGEFPHBog" );
				twitterRequest.setConsumerApiSecretKey( "kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw" );
				twitterRequest.setConsumerAccessToken( "370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb" );
				twitterRequest.setConsumerAccessTokenSecret( "LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE" );
				twitterRequest.setParams( {
					  "status"           = "Hello Ladies + Gentlemen, a signed OAuth request!"
					, "include_entities" = true
				} );

				utils.$( "_generateNonce", "kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg" );
				utils.$( "_getTimestamp", "1318622958" );

				var expectedResult = 'OAuth oauth_consumer_key="xvz1evFS4wEEPTGEFPHBog", oauth_nonce="kYjzVBB8Y0ZFabxSWbWovY3uYSQ2pTgmZeNu2VS4cg", oauth_signature="hCtSmYh%2BiHYCEqBWrE7C7hYmtUk%3D", oauth_signature_method="HMAC-SHA1", oauth_timestamp="1318622958", oauth_token="370773112-GmHxMAgYyLbNEtIKZeRNFsMKPR9EyMZeS9weJAEb", oauth_version="1.0"';

				utils.buildAuthHeader( twitterRequest );
				expect( twitterRequest.getAuthHeader() ).toBe( expectedResult );
			} );

		});

	}




	private function _getUtils() {
		var utils = new twitter.services.TwitterUtils();
			utils = createMock( object=utils );

		return utils;
	}

	private function _newTwitterRequest() {
		var utils = _getUtils();

		return new twitter.services.TwitterRequest( utils=utils );
	}

}