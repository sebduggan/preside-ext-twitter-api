/**
 * @nolabel                         true
 * @versioned                       false
 * @dataManagerGroup                Twitter
 * @dataManagerGridFields           id,label,account,date_posted
 * @dataManagerDefaultSortOrder     date_posted desc
 * @dataManagerDisallowedOperations add,edit,clone
 */

component {
	property name="id"             type="string"  dbtype="varchar" maxlength=30 generator="none" required=true sortorder=1;
	property name="account"        relationship="many-to-one" relatedTo="twitter_account" required=true oncascade="delete";
	property name="date_posted"    type="date"    dbtype="datetime" indexes="DatePosted";
	property name="text"           type="string"  dbtype="varchar" maxlength=500;
	property name="location"       type="string"  dbtype="varchar" maxlength=250;
	property name="source"         type="string"  dbtype="varchar" maxlength=250;
	property name="rendered_body"  adminViewGroup="rendered" type="string"  dbtype="text";
	property name="rendered_media" adminViewGroup="rendered" type="string"  dbtype="text";
	property name="is_reply"       type="boolean" dbtype="boolean" default=false indexes="isReply";
	property name="is_retweet"     type="boolean" dbtype="boolean" default=false indexes="isRetweet";
	property name="quote_count"    adminViewGroup="stats" type="numeric" dbtype="int";
	property name="reply_count"    adminViewGroup="stats" type="numeric" dbtype="int";
	property name="retweet_count"  adminViewGroup="stats" type="numeric" dbtype="int";
	property name="favorite_count" adminViewGroup="stats" type="numeric" dbtype="int";

	property name="raw_data"       adminViewGroup="technical" type="string" dbtype="text";
	property name="label"          adminRenderer="none"       type="string" formula="case when length( ${prefix}text ) > 75 then concat( left( ${prefix}text, 75 ), '...' ) else ${prefix}text end";
}