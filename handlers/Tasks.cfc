/**
 * Twitter-specific tasks for the task manager
 *
 */
component {
	property name="twitterSyncService" inject="twitterSyncService";

	/**
	 * Get new tweets
	 *
	 * @displayName  Get new tweets
	 * @displayGroup Twitter
	 * @schedule     0 *\/10 * * * *
	 * @priority     10
	 * @timeout      1200
	 */
	public boolean function getTweets( any logger ) {
		return twitterSyncService.getTweets(
			logger  = arguments.logger ?: nullValue()
		);
	}

	/**
	 * Re-render tweets
	 *
	 * @displayName  Re-render tweets
	 * @displayGroup Twitter
	 * @schedule     disabled
	 * @priority     10
	 * @timeout      1200
	 */
	public boolean function reRenderTweets( any logger ) {
		return twitterSyncService.reRenderTweets(
			logger  = arguments.logger ?: nullValue()
		);
	}

	/**
	 * Update counts/deletions of tweets posted in last day
	 *
	 * @displayName  Update counts/deletions (1 day)
	 * @displayGroup Twitter
	 * @schedule     0 *\/10 * * * *
	 * @priority     10
	 * @timeout      1200
	 */
	public boolean function updateCounts1Day( any logger ) {
		return twitterSyncService.updateCounts(
			  logger = arguments.logger ?: nullValue()
			, days   = 1
		);
	}

	/**
	 * Update counts/deletions of tweets posted in last 30 days
	 *
	 * @displayName  Update counts/deletions (30 days)
	 * @displayGroup Twitter
	 * @schedule     0 0 *\/2 * * *
	 * @priority     10
	 * @timeout      1200
	 */
	public boolean function updateCounts30Day( any logger ) {
		return twitterSyncService.updateCounts(
			  logger = arguments.logger ?: nullValue()
			, days   = 30
		);
	}

	/**
	 * Update counts/deletions of all tweets
	 *
	 * @displayName  Update counts/deletions (all)
	 * @displayGroup Twitter
	 * @schedule     1 0 0 * * *
	 * @priority     10
	 * @timeout      1200
	 */
	public boolean function updateCountsAll( any logger ) {
		return twitterSyncService.updateCounts(
			  logger = arguments.logger ?: nullValue()
			, days   = 0
		);
	}

}