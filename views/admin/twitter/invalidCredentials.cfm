<cfoutput>
	<div class="clearfix alert alert-block alert-danger">
		<p>
			<i class="fa fa-hand-stop-o fa-lg fa-fw"></i>
			#translateResource( "twitter:oauth.invalid.credentials" )#
		</p>
	</div>
</cfoutput>