component extends="testbox.system.BaseSpec" {

	function run(){

		describe( "getSigningKey", function(){

			it( "should return a concatenated, percent-encoded signing key", function(){
				var twitterRequest     = _newTwitterRequest();
				var expectedSigningKey = "kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw&LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE";

				twitterRequest.setConsumerApiSecretKey( "kAcSOqF21Fu85e7zjz7ZN2U4ZRhfV3WpwPAoE3Z7kBw" );
				twitterRequest.setConsumerAccessTokenSecret( "LswwdoUaIvS8ltyTt5jkRh4J50vUPVVHtR2YPi5kE" );

				expect( twitterRequest.getSigningKey() ).toBe( expectedSigningKey );
			} );

		});

	}



	private function _getUtils() {
		var utils = new twitter.services.TwitterUtils();
			utils = createMock( object=utils );

		return utils;
	}

	private function _newTwitterRequest() {
		var utils = _getUtils();

		return new twitter.services.TwitterRequest( utils=utils );
	}

}