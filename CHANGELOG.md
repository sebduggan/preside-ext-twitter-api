# Changelog

## v1.0.1

* Force login when authorizing a Twitter account to ensure correct user is selected

## v1.0.0

* Refactor authentication to use user-initiated OAuth, allowing multiple Twitter accounts to be set up with a single dev app

## v0.7.3

* Add default ids to twitter-feed form

## v0.7.2

* Ensure correct encoding of API params in http call
* Log API connection errors

## v0.7.1

* Better handling of line breaks in rendered tweets

## v0.7.0

* Add `postStatus()` method to allow posting of Tweets
* Rework `percentEncode()` to work with high-codepoint unicode characters

## v0.6.2

* Fix typo in documentation
* Include JS in default tweet renderer

## v0.6.1

* Add documentation for the Twitter feed widget

## v0.6.0

* Add simple twitter feed widget

## v0.5.7

* Hide included SVG sprites

## v0.5.6

* Attempt to fix sync error

## v0.5.5

* Fix regression in TwitterRequest
* Remove debug code
* Add nested rendering of quoted tweets

## v0.5.4

* Add documentation to the public service methods
* Add tweet background as Sass variable
* Add icon & link to "replying to" element
* Add avatar to tweet user element

## v0.5.3

* Remove debug code...

## v0.5.2

* Minor tweet layout changes
* Remove check for site's http/s status: Twitter redirects all requests to https anyway...
* Add rendering for separate "Replying to..." component

## v0.5.1

* Unique IDs for each rendered tweet's media
* Add error handling to API calls and tasks

## v0.5.0

* Better rendering of retweets
* Display datetimes in the past hour as minutes

## v0.4.4

* Change summary to shortDescription in box.json

## v0.4.3

* Fix query filter in rerender tweets task
* Better logging in rerender task

## v0.4.2

* Sort tweet data grid by date_posted
* Add indexes to twitter_status object

## v0.4.1

* Store IDs as strings, sort tweets by date_posted

## v0.4.0

* Update documentation
* Set summary for Forgebox

## v0.3.2

* Move display text to i18n properties
* Localise formatted date display
* Accessibility improvements (ARIA)

## v0.3.1

* Refactor CSS source structure
* Refactor public rendering methods

## v0.3.0

* Add web intents for user links
* Add SVG icons for tweet actions
* Refactor tweet renderers for better customisation

## v0.2.0

* Add styles for the default rendered tweets
* Add metadata and Twitter actions to default renderers
* Add lightbox for viewing media items
* Add JS interaction for Twitter intent actions

## v0.1.2

* Rework entity replacement to prevent potential double-replacements

## v0.1.1

* Replace newlines with HTML line breaks in default tweet renderer

## v0.1.0

* Release of initial functionality
	- Configure multiple Twitter accounts with credentials
	- Scheduled tasks to import existing tweets, and run regularly to pick up new ones
	- Scheduled tasks to update like/retweet counts, and to remove deleted tweets
	- Default renderers to convert the API data into a fully-rendered tweet (currently no default styling or interaction handling)
	- Manual task to re-render all saved tweets (e.g. if the renderers have changed)