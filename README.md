# Twitter API Integration for Preside

This extension allows you to configure multiple Twitter accounts in your Preside application, and to interact with the Twitter API.

## Benefits

- Custom rendering of timelines and individual tweets: tweets can be placed and formatted however desired
- Speed of rendering: tweets are rendered as part of the page (and can be cached if desired), with no external requests to Twitter
- Indexing: tweets can be indexed and searched both locally and by search engines
- No tracking: because no 3rd-party widgets are being rendered by your page, no 3rd-party tracking is happening either

## Documentation

Full documentation for installation and usage of this extension can be found on the [project's wiki](https://bitbucket.org/sebduggan/preside-ext-twitter-api/wiki/).