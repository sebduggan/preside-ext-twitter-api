component extends="coldbox.system.Interceptor" {

	property name="dsn"                  inject="coldbox:setting:dsn";
	property name="sqlRunner"            inject="provider:sqlRunner";
	property name="dbInfoService"        inject="provider:dbInfoService";
	property name="presideObjectService" inject="delayedInjector:presideObjectService";

// PUBLIC
	public void function configure() {}

	public void function postPresideReload( event, interceptData ) {
		var accountsToMigrate = sqlRunner.runSql( sql="select * from pobj_twitter_account where developer_app is null", dsn=dsn );

		if ( !accountsToMigrate.recordCount ) {
			return;
		}

		var accountDao = presideObjectService.getObject( "twitter_account" );
		var devAppDao  = presideObjectService.getObject( "twitter_developer_app" );
		var devAppId   = "";

		for( var account in accountsToMigrate ) {
			var apiKey    = account.__deprecated__consumer_api_key        ?: "";
			var apiSecret = account.__deprecated__consumer_api_secret_key ?: "";

			if ( !len( apiKey ) || !len( apiSecret ) ) {
				continue;
			}

			devAppId = devAppDao.selectData( filter={ consumer_api_key=apiKey, consumer_api_secret_key=apiSecret } ).id;
			if ( !len( devAppId ) ) {
				devAppId = devAppDao.insertData( data={
					  label                   = "#account.screen_name# app"
					, consumer_api_key        = apiKey
					, consumer_api_secret_key = apiSecret
				} );
			}

			accountDao.updateData( id=account.id, data={ developer_app=devAppId } );
		}
	}

}