#!/bin/bash

cd `dirname $0`
exitcode=0

if [ ! -d "`dirname $0`/tests/testbox" ]; then
  box install
fi

box stop name="twitterApiTests"
box start directory="./tests/" serverConfigFile="./server-tests.json"
cd tests
box testbox run runner="/runner.cfm" verbose=false || exitcode=1
box stop name="twitterApiTests"

exit $exitcode
