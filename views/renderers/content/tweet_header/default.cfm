<cfscript>
	isRetweet = !isEmpty( args.tweetData.retweeted_status ?: {} );
	tweetData = isRetweet ? args.tweetData.retweeted_status : args.tweetData;
	retweeter = isRetweet ? args.tweetData.user : {};
</cfscript>
<cfoutput>
	<div class="tweet-header">
		<cfif isRetweet>
			<div class="tweet-header-retweeter">
				<svg role="presentation" aria-hidden="true"><use xlink:href="##tweet-action-retweet" /></svg>
				<a href="https://twitter.com/intent/user?user_id=#retweeter.id_str#" rel="noopener noreferrer" target="_blank">Retweeted by #retweeter.name#</a>
			</div>
		</cfif>
		<div class="tweet-header-meta">
			<a href="https://twitter.com/intent/user?user_id=#tweetData.user.id_str#" class="tweet-user" rel="noopener noreferrer" target="_blank" aria-hidden="true"><img src="#tweetData.user.profile_image_url_https#"> <span class="tweet-user-name">#tweetData.user.name#</span> <span class="tweet-user-screen-name">@#tweetData.user.screen_name#</span></a>
			<a href="https://twitter.com/#tweetData.user.screen_name#/status/#tweetData.id_str#" class="tweet-date" rel="noopener noreferrer" target="_blank" title="#lsDatetimeformat( tweetData.created_at, "h:mm TT • d mmm, yyyy" )#">#renderTweetDate( tweetData.created_at )#</a>
		</div>
	</div>
</cfoutput>