<cfscript>
	uuid           = args.itemData.uuid      ?: "";
	mediaSize      = args.itemData.count == 1 ? "small" : "360x360";
	mediaUrl       = args.itemData.url       ?: "";
	mediaExtension = args.itemData.extension ?: "";
	mediaBaseUrl   = args.itemData.baseUrl   ?: "";
	altText        = args.ext_alt_text       ?: "";
	image          = "#mediaBaseUrl#?format=#mediaExtension#&name=#mediaSize#";
	viewLabel      = translateResource( uri="twitter:view.image.label" );
	uncaptioned    = translateResource( uri="twitter:uncaptioned.image.label" );
</cfscript>
<cfoutput>
	<a href="#mediaUrl#"
		data-mediabox="tweet-gallery-#uuid#"
		class="tweet-media-item"
		aria-label="#viewLabel#"
		rel="noopener noreferrer"
		target="_blank">
		<div class="tweet-media-item-bg" style="background-image: url(#image#);"></div>
		<img src="#image#" alt="#len( altText ) ? altText : uncaptioned#">
	</a>
</cfoutput>