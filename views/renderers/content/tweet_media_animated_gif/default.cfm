<cfscript>
	uuid           = args.itemData.uuid      ?: "";
	mediaUrl       = args.itemData.url       ?: "";
	mediaExtension = args.itemData.extension ?: "";
	mediaBaseUrl   = args.itemData.baseUrl   ?: "";
	altText        = args.ext_alt_text       ?: "";
	width          = args.sizes.large.w      ?: 400;
	height         = args.sizes.large.h      ?: 400;
	videoUrl       = args.video_info.variants[ 1 ].url ?: ""
	image          = "#mediaBaseUrl#?format=#mediaExtension#&name=small";
	playVideoLabel = translateResource( uri="twitter:play.video.label" );
	uncaptioned    = translateResource( uri="twitter:uncaptioned.video.label" );
</cfscript>
<cfoutput>
	<a href="#videoUrl#"
		data-mediabox="tweet-gallery-#uuid#"
		data-iframe="true"
		data-width="#width#"
		data-height="#height#"
		class="tweet-media-item"
		rel="noopener noreferrer"
		aria-label="#playVideoLabel#"
		target="_blank">
		<img src="#image#" alt="#len( altText ) ? altText : uncaptioned#">
		<div class="tweet-media-video-play">
			<svg role="img" aria-label="#playVideoLabel#"><use xlink:href="##tweet-media-video-play" /></svg>
		</div>
	</a>
</cfoutput>
