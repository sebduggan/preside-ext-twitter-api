/**
 * @labelField                      screen_name
 * @versioned                       false
 * @datamanagerGridFields           screen_name,user_id,active,timeline_start_date,datecreated,datemodified
 * @dataManagerGroup                Twitter
 * @dataManagerDisallowedOperations clone
 */

component {
	property name="developer_app"           relationship="many-to-one" relatedTo="twitter_developer_app" quickadd=true;

	property name="screen_name"             type="string"  dbtype="varchar" maxlength=100 uniqueindexes="twitterScreenName";
	property name="user_id"                 type="string"  dbtype="varchar" maxlength=20;
	property name="active"                  type="boolean" dbtype="boolean" default=false;
	property name="timeline_start_date"     type="date"    dbtype="date";

	property name="access_token"            adminViewGroup="oauth" type="string"  dbtype="varchar" maxlength=100;
	property name="access_token_secret"     adminViewGroup="oauth" type="string"  dbtype="varchar" maxlength=100;
}
