<cfscript>
	name       = args.name        ?: "";
	screenName = args.screen_name ?: "";
	userId     = args.id_str      ?: "";
</cfscript>
<cfoutput>
	<a href="https://twitter.com/intent/user?user_id=#userId#" rel="noopener noreferrer" target="_blank" title="#htmlEditFormat( name )#">@#screenName#</a>
</cfoutput>