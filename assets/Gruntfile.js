module.exports = function( grunt ) {

	var sass = require('sass');

	grunt.loadNpmTasks( 'grunt-contrib-clean'  );
	grunt.loadNpmTasks( 'grunt-contrib-cssmin' );
	grunt.loadNpmTasks( 'grunt-sass'           );
	grunt.loadNpmTasks( 'grunt-contrib-rename' );
	grunt.loadNpmTasks( 'grunt-contrib-uglify' );
	grunt.loadNpmTasks( 'grunt-rev'            );
	grunt.loadNpmTasks( 'grunt-postcss'        );
	grunt.loadNpmTasks( 'grunt-svgstore'       );

	grunt.registerTask( 'default', [ 'uglify', 'sass', 'postcss', 'cssmin', 'svgstore', 'clean', 'rev', 'rename' ] );

	grunt.initConfig( {
		uglify: {
			options:{
				  sourceMap     : true
				, sourceMapName : function( dest ){
					var parts = dest.split( "/" );
					parts[ parts.length-1 ] = parts[ parts.length-1 ].replace( /\.js$/, ".map" );
					return parts.join( "/" );
				 }
			},
			twitter:{
				files: [{
					expand  : true,
					cwd     : "js/",
					src     : ["**/*.js", "!**/_*.min.js", "!lib/*" ],
					dest    : "js/",
					ext     : ".min.js",
					rename  : function( dest, src ){
						var pathSplit = src.split( '/' );

						pathSplit[ pathSplit.length-1 ] = "_" + pathSplit[ pathSplit.length-2 ] + ".min.js";

						return dest + pathSplit.join( "/" );
					}
				}]
			}
		},

		sass: {
			twitter: {
				options: {
					implementation: sass,
					sourceMap: true,
					style: 'nested'
				},
				files: {
					'css/twitter/twitter.scss.css': 'css/twitter/twitter.scss'
				}
			}
		},

		postcss: {
			options: {
				processors : [
					require( 'autoprefixer' )()
				]
			},
			all: {
				src  : 'css/**/*.scss.css'
			}
		},

		cssmin: {
			all: {
				expand : true,
				cwd    : 'css/',
				src    : [ '**/*.css', '!**/_*.min.css' ],
				ext    : '.min.css',
				dest   : 'css/',
				rename : function( dest, src ){
					var pathSplit = src.split( '/' );

					pathSplit[ pathSplit.length-1 ] = "_" + pathSplit[ pathSplit.length-2 ] + ".min.css";
					return dest + pathSplit.join( "/" );
				}
			}
		},

		svgstore: {
			options: {
				svg : {
					xmlns         : "http://www.w3.org/2000/svg",
					version       : "1.1",
					"aria-hidden" : true,
					style         : "display:none;"
				},
				formatting : {
					indent_size : 2
				}
			},
			all: {
				files: {
					'../views/twitter/_svgs.cfm': ['svg/*.svg'],
				},
			}
		},

		clean: {
			all : {
				files : [{
					  src    : "**/_*.min.{js,css}"
					, filter : function( src ){ return src.match(/[\/\\]_[a-f0-9]{8}\./) !== null; }
				}]
			}
		},

		rev: {
			options: {
				algorithm : 'md5',
				length    : 8
			},
			all: {
				files : [
					  { src : "js/**/_*.min.js"  }
					, { src : "css/**/_*.min.css" }
				]
			}
		},

		rename: {
			assets: {
				expand : true,
				cwd    : '',
				src    : '**/*._*.min.{js,css}',
				dest   : '',
				rename : function( dest, src ){
					var pathSplit = src.split( '/' );

					pathSplit[ pathSplit.length-1 ] = "_" + pathSplit[ pathSplit.length-1 ].replace( /\._/, "." );

					return dest + pathSplit.join( "/" );
				}
			}
		},

		browserslist: [
			"defaults",
			"IE >= 9"
		]
	} );
};