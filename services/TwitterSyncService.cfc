/**
 * @presideService
 * @singleton
 */
component {
// CONSTRUCTOR
	/**
	 * @apiWrapper.inject        TwitterApiWrapper
	 * @renderingService.inject  TwitterRenderingService
	 */
	public any function init( required any apiWrapper, required any renderingService ) {
		_setApiWrapper( arguments.apiWrapper );
		_setRenderingService( arguments.renderingService );
		return this;
	}

//PUBLIC METHODS
	/**
	 * Retrieves all newly-posted tweets for each active account, and store them locally along
	 * with default-rendered body and media.
	 *
	 * If no tweets are currently stored for an account, will retrieve ALL tweets for that account
	 * (or back as far as the account's timeline start date, if configured).
	 *
	 * Returns true if successful, false if an error is encountered.
	 *
	 * @logger  An optional logger object (will be passed automatically by a scheduled task)
	 */
	public boolean function getTweets( any logger ) {
		var hasLogger = StructKeyExists( arguments, "logger" );
		var canInfo   = hasLogger && arguments.logger.canInfo();
		var canWarn   = hasLogger && arguments.logger.canWarn();
		var canError  = hasLogger && arguments.logger.canError();

		var accounts       = _getAccounts();
		var dao            = $getPresideObject( "twitter_status" );
		var tweets         = [];
		var tweetData      = {};
		var maxTweetId     = "";
		var sinceTweetId   = "";
		var params         = {};
		var twitterRequest = "";
		var importComplete = false;

		if ( canInfo ) { arguments.logger.info( "#accounts.recordcount# accounts(s) found..." ); }

		for( var account in accounts ) {
			if ( canInfo ) { arguments.logger.info( "Get new tweets for [#account.screen_name#]" ); }

			maxTweetId     = "";
			sinceTweetId   = _getLatestTweetIdForAccount( account.id );
			importComplete = false;

			while( !importComplete ) {
				params = {
					  screen_name          = account.screen_name
					, count                = 200
					, tweet_mode           = "extended"
					, include_ext_alt_text = true
				};
				if ( len( maxTweetId ) ) {
					params.max_id = precisionEvaluate( "#maxTweetId# - 1" );
				}
				if ( !isnull( sinceTweetId ) ) {
					params.since_id = sinceTweetId;
				}

				twitterRequest = _getApiWrapper().getUserTimeline(
					  account = account
					, params  = params
				);

				if ( twitterRequest.getSuccess() ) {
					tweets = twitterRequest.getResponse();

					if ( canInfo ) { arguments.logger.info( "Retrieved #tweets.len()# tweet(s)" ); }

					if ( !tweets.len() ) {
						break;
					}

					for( tweetData in tweets ) {
						if ( isDate( account.timeline_start_date ) && parseDateTime( tweetData.created_at ) < account.timeline_start_date  ) {
							if ( canInfo ) { arguments.logger.info( "Timeline start date reached. Stopping..." ); }
							importComplete = true;
							break;
						}

						try {
							maxTweetId = tweetData.id_str;
							dao.insertData( {
								  id             = tweetData.id_str
								, account        = account.id
								, date_posted    = parseDateTime( tweetData.created_at )
								, text           = tweetData.full_text       ?: ""
								, location       = tweetData.place.full_name ?: ""
								, source         = tweetData.source          ?: ""
								, rendered_body  = _getRenderingService().renderTweetBody( tweetData=tweetData )
								, rendered_media = _getRenderingService().renderTweetMedia( tweetData=tweetData )
								, is_retweet     = tweetData.retweeted       ?: false
								, is_reply       = !isEmpty( tweetData.in_reply_to_status_id ?: "" )
								, quote_count    = tweetData.quote_count     ?: 0
								, reply_count    = tweetData.reply_count     ?: 0
								, retweet_count  = tweetData.retweet_count   ?: 0
								, favorite_count = tweetData.favorite_count  ?: 0
								, raw_data       = serializeJson( tweetData )
							} );
						}
						catch( e ) {
							if ( e.NativeErrorCode ?: "" == 1062 ) {
								if ( canWarn ) { arguments.logger.warn( "Attempted to insert duplicate ID [#tweetData.id_str#]" ); }
							} else {
								rethrow;
							}
						}
					}
				} else {
					if ( canError ) { arguments.logger.error( "Failed to retrieve data from Twitter. [#twitterRequest.getResponse()#]" ); }
					return false;
				}
			}

			if ( canInfo ) { arguments.logger.info( "Finish getting tweets for [#account.screen_name#]" ); }
		}
		if ( canInfo ) { arguments.logger.info( "All finished." ); }

		return true;
	}

	/**
	 * Re-runs the default renderers for all tweets stored locally, and persists the rendered
	 * body and media content.
	 *
	 * This is useful if the renderers have been modified and the stored body or media content
	 * no longer matches the templates.
	 *
	 * Returns true if successful, false if an error is encountered.
	 *
	 * @logger  An optional logger object (will be passed automatically by a scheduled task)
	 */
	public boolean function reRenderTweets( any logger ) {
		var hasLogger = StructKeyExists( arguments, "logger" );
		var canInfo   = hasLogger && arguments.logger.canInfo();
		var canWarn   = hasLogger && arguments.logger.canWarn();
		var canError  = hasLogger && arguments.logger.canError();

		var accounts  = _getAccounts();
		var dao       = $getPresideObject( "twitter_status" );
		var tweets    = "";
		var tweet     = "";
		var tweetData = {};

		if ( canInfo ) { arguments.logger.info( "#accounts.recordcount# accounts(s) found..." ); }

		for( var account in accounts ) {
			if ( canInfo ) { arguments.logger.info( "Re-render tweets for [#account.screen_name#]" ); }

			tweets = dao.selectData(
				  filter       = { account=account.id }
				, selectFields = [ "id", "raw_data" ]
				, orderBy      = "date_posted desc"
			);

			if ( canInfo ) { arguments.logger.info( "#tweets.recordcount# tweet(s) found" ); }

			for( tweet in tweets ) {
				tweetData = deserializeJson( tweet.raw_data );
				dao.updateData( id=tweet.id, data={
					  rendered_body  = _getRenderingService().renderTweetBody( tweetData=tweetData )
					, rendered_media = _getRenderingService().renderTweetMedia( tweetData=tweetData )
				} );

				if ( canInfo ) {
					if ( tweets.currentRow == tweets.recordCount ) {
						arguments.logger.info( "Processed #tweets.currentRow mod 100# tweet(s)" );
					} else if ( tweets.currentRow mod 100 == 0 ) {
						arguments.logger.info( "Processed 100 tweet(s)" );
					}
				}
			}

			if ( canInfo ) { arguments.logger.info( "Finish re-rendering tweets for [#account.screen_name#]" ); }
		}
		if ( canInfo ) { arguments.logger.info( "All finished." ); }

		return true;
	}

	/**
	 * Retrieves updated stats for all tweets posted within the specified number of days and persists them
	 * locally.
	 *
	 * If a tweet is reported as no longer existing, it will be deleted from the local store.
	 *
	 * Returns true if successful, false if an error is encountered.
	 *
	 * @days    The number of days to check the stats for. If set to 0 (default), all tweets will be updated
	 * @logger  An optional logger object (will be passed automatically by a scheduled task)
	 */
	public boolean function updateCounts( numeric days=0, any logger ) {
		var hasLogger = StructKeyExists( arguments, "logger" );
		var canInfo   = hasLogger && arguments.logger.canInfo();
		var canWarn   = hasLogger && arguments.logger.canWarn();
		var canError  = hasLogger && arguments.logger.canError();

		var accounts  = _getAccounts();
		var dao       = $getPresideObject( "twitter_status" );
		var lookups   = {};
		var tweet     = {};
		var tweetIds  = [];

		if ( canInfo ) { arguments.logger.info( "#accounts.recordcount# accounts(s) found..." ); }

		for( var account in accounts ) {
			if ( canInfo ) { arguments.logger.info( "Start updating tweet stats for [#account.screen_name#]" ); }

			tweetIds = [ "" ];

			while( true ) {
				tweetIds = _getTweetIdsToUpdate( accountId=account.id, days=arguments.days, beforeId=tweetIds.last() );

				if ( !tweetIds.len() ) {
					break;
				}

				if ( canInfo ) { arguments.logger.info( "Updating #tweetIds.len()# tweet(s)" ); }

				var twitterRequest = _getApiWrapper().getStatuses(
					  account = account
					, params  = {
						  "id"                   = tweetIds.toList()
						, "include_entities"     = false
						, "trim_user"            = true
						, "map"                  = true
						, "include_ext_alt_text" = false
						, "include_card_uri"     = false
					}
				);

				if ( twitterRequest.getSuccess() ) {
					lookups = twitterRequest.getResponse();

					for( var id in ( lookups.id ?: {} ) ) {
						if ( isNull( lookups.id[ id ] ) ) {
							dao.deleteData( id=id );
							if ( canWarn ) { arguments.logger.warn( "Deleted tweet ID [#id#]" ); }
						} else {
							tweet = lookups.id[ id ];
							dao.updateData( id=tweet.id_str, data={
								  quote_count    = tweet.quote_count    ?: 0
								, reply_count    = tweet.reply_count    ?: 0
								, retweet_count  = tweet.retweet_count  ?: 0
								, favorite_count = tweet.favorite_count ?: 0
							} );
						}
					}
				} else {
					if ( canError ) { arguments.logger.error( "Failed to retrieve data from Twitter. [#twitterRequest.getResponse()#]" ); }
					return false;
				}
			}

			if ( canInfo ) { arguments.logger.info( "Finish updating tweet stats for [#account.screen_name#]" ); }
		}

		if ( canInfo ) { arguments.logger.info( "All finished." ); }

		return true;
	}

// PRIVATE METHODS
	private numeric function _getLatestTweetIdForAccount( required string accountId ) {
		var latest = $getPresideObject( "twitter_status" ).selectData(
			  filter       = { "account.id"=arguments.accountId }
			, orderBy      = "twitter_status.date_posted desc"
			, maxRows      = 1
			, selectFields = [ "twitter_status.id" ]
		);

		return latest.recordcount ? latest.id : nullvalue();
	}

	private array function _getTweetIdsToUpdate( required string accountId, required numeric days, required string beforeId ) {
		var filter       = "account.id = :account.id";
		var filterParams = { "account.id"=arguments.accountId };

		if ( arguments.days ) {
			filter &= " and date_posted > :date_posted";
			filterParams[ "date_posted" ] = dateAdd( "d", -arguments.days, now() );
		}
		if ( len( arguments.beforeId ) ) {
			filter &= " and twitter_status.id < :twitter_status.id";
			filterParams[ "twitter_status.id" ] = arguments.beforeId;
		}

		var tweets = $getPresideObject( "twitter_status" ).selectData(
			  filter       = filter
			, filterParams = filterParams
			, orderBy      = "twitter_status.date_posted desc"
			, maxRows      = 100
			, selectFields = [ "twitter_status.id" ]
		);

		return tweets.valueArray( "id" );
	}

	private query function _getAccounts() {
		return $getPresideObject( "twitter_account" ).selectData(
			  filter            = { active=true }
			, extraSelectFields = [ "developer_app.consumer_api_key", "developer_app.consumer_api_secret_key" ]
		);
	}

 // GETTERS AND SETTERS
	private any function _getApiWrapper() {
		return _apiWrapper;
	}
	private void function _setApiWrapper( required any apiWrapper ) {
		_apiWrapper = arguments.apiWrapper;
	}

	private any function _getRenderingService() {
		return _renderingService;
	}
	private void function _setRenderingService( required any renderingService ) {
		_renderingService = arguments.renderingService;
	}

}