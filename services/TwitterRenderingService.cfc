/**
 * @presideService
 * @singleton
 */
component {
// CONSTRUCTOR
	public any function init() {
		return this;
	}

//PUBLIC METHODS
	/**
	 * Retrieves a tweet from the database by ID and renders it using the supplied context.
	 * If the context is default, it will use the persisted rendered content for the body and media sections.
	 *
	 * @id       The numeric ID of the tweet
	 * @context  The context to use for the renderers
	 */
	public string function renderTweet(
		  required string id
		,          string context = "default"
	) {
		var tweetRecord = _getTweetById( arguments.id );
		var tweet       = {
			  tweetData    = deserializeJson( tweetRecord.raw_data )
			, replyCount   = val( tweetRecord.reply_count )
			, retweetCount = val( tweetRecord.retweet_count )
			, likeCount    = val( tweetRecord.favorite_count )
		};
		if ( _useDefaultContext( "tweet_body", arguments.context ) ) {
			tweet.renderedBody  = tweetRecord.rendered_body;
		}
		if ( _useDefaultContext( "tweet_media", arguments.context ) ) {
			tweet.renderedMedia = tweetRecord.rendered_media;
		}

		return _renderTweet( argumentCollection=tweet, context=arguments.context );
	}

	/**
	 * Retrieves a collection of tweets from the database and renders them using the supplied context.
	 * Use any valid selectData() arguments to define the tweet collection.
	 *
	 * @maxRows  Defaults to 10 as a protection!
	 * @orderBy  Defaults to latest tweets first
	 * @context  The context to use for the renderers
	 */
	public string function renderTweets(
		  numeric maxRows = 10
		, string  orderBy = "date_posted desc"
		, string  context = "default"
	) {
		var tweets         = _selectTweets( argumentCollection=arguments );
		var tweet          = {};
		var renderedTweets = [];

		for( var tweetRecord in tweets ) {
			tweet = {
				  tweetData    = deserializeJson( tweetRecord.raw_data )
				, replyCount   = val( tweetRecord.reply_count )
				, retweetCount = val( tweetRecord.retweet_count )
				, likeCount    = val( tweetRecord.favorite_count )
			};
			if ( _useDefaultContext( "tweet_body", arguments.context ) ) {
				tweet.renderedBody  = tweetRecord.rendered_body;
			}
			if ( _useDefaultContext( "tweet_media", arguments.context ) ) {
				tweet.renderedMedia = tweetRecord.rendered_media;
			}

			renderedTweets.append( _renderTweet( argumentCollection=tweet, context=arguments.context ) );
		}

		return renderedTweets.toList( chr( 10 ) );
	}

	/**
	 * Renders the body of a tweet using the supplied context, with linked entities.
	 * This will not change over time, and so the default rendering can be persisted to the database.
	 *
	 * @tweetData  The raw, deserialized tweet object as returned by the API
	 * @context    The context to use for the renderers
	 * @isQuoted   Are we rendering a nested, quoted status?
	 */
	public string function renderTweetBody( required struct tweetData, string context="default", boolean isQuoted=false ) {
		var isRetweet       = !isEmpty( arguments.tweetData.retweeted_status ?: {} );
		var tweetData       = isRetweet ? arguments.tweetData.retweeted_status : arguments.tweetData;
		var entities        = _combineAndSortEntities( tweetData );
		var renderedBody    = tweetData.full_text ?: "";
		var renderedReplies = "";
		var stringToReplace = "";
		var placeholder     = "";
		var placeholders    = {};
		var inReplyToCount  = 0;
		var inReplyTo       = [];
		var removeQuoteLink = !isEmpty( tweetData.quoted_status ?: {} ) && !arguments.isQuoted;
		var quoteLink       = removeQuoteLink ? ( tweetData.quoted_status_permalink.url ?: "" ) : "";

		for( var entity in entities ) {
			stringToReplace = _getStringToReplace( entity );
			placeholder     = "";
			if ( entity.type != "media" && stringToReplace != quoteLink ) {
				placeholder = "{#createuuid()#}";
				placeholders[ placeholder ] = _renderEntity( entity, arguments.context );
			}

			renderedBody = replacenocase( renderedBody, stringToReplace, placeholder, "one" );
		}

		inReplyToCount = _getInReplyToCount( tweetData );
		if ( inReplyToCount ) {
			for( var i=1; i<=inReplyToCount; i++ ) {
				inReplyTo.append( listFirst( renderedBody, " " ) );
				renderedBody = listRest( renderedBody, " " );
			}
			renderedReplies = $renderContent(
				  renderer = "tweet_in_reply_to"
				, data     = {
					  inReplyTo           = inReplyTo
					, inreplyToStatus     = tweetData.in_reply_to_status_id_str ?: ""
					, inReplyToScreenName = tweetData.in_reply_to_screen_name   ?: ""
				  }
				, context  = arguments.context
			);
		}

		renderedBody    = replace( ltrim( renderedBody ), placeholders );
		renderedReplies = replace( renderedReplies, placeholders );

		return trim( $renderContent( renderer="tweet_body", data={ tweetData=tweetData, renderedBody=renderedBody, renderedReplies=renderedReplies }, context=arguments.context ) );
	}

	/**
	 * Renders the media attached to a tweet using the supplied context.
	 * This will not change over time, and so the default rendering can be persisted to the database.
	 *
	 * @tweetData  The raw, deserialized tweet object as returned by the API
	 * @context    The context to use for the renderers
	 */
	public string function renderTweetMedia( required struct tweetData, string context="default" ) {
		var isRetweet     = !isEmpty( arguments.tweetData.retweeted_status ?: {} );
		var tweetData     = isRetweet ? arguments.tweetData.retweeted_status : arguments.tweetData;
		var mediaEntities = _combineMediaEntities( tweetData );
		var media         = [];
		var mediaType     = "";

		for( var mediaEntity in mediaEntities ) {
			mediaType = mediaEntity.data.type;
			media.append( _renderEntity( mediaEntity, arguments.context ) );
		}

		return trim( $renderContent( renderer="tweet_media", data={ tweetData=tweetData, media=media, mediaType=mediaType }, context=arguments.context ) );
	}

	/**
	 * Renders a tweet date differently depending on how long ago it is:
	 * Datetimes in the past hour will be displayed as a number of minutes (e.g. "32m")
	 * Datetimes in the past 24 hours will be displayed as a number of hours (e.g. "14h")
	 * Datetimes in the current year will be displayed as date and month (e.g. "12 Jan")
	 * Datetimes older than that will be also have a year appended (e.g. "24 Nov, 2019")
	 *
	 * @date     The date to format
	 * @context  The context to use for the renderers
	 */
	public string function renderTweetDate( required date date ) {
		var dateMask   = "d mmm";
		var parsedDate = parseDateTime( arguments.date );

		if ( dateDiff( "n", parsedDate, now() ) < 60 ) {
			var minuteSuffix = $translateResource( uri="twitter:minuteSuffix" );
			return dateDiff( "n", parsedDate, now() ) & minuteSuffix;
		}
		if ( dateDiff( "h", parsedDate, now() ) < 24 ) {
			var hourSuffix = $translateResource( uri="twitter:hourSuffix" );
			return dateDiff( "h", parsedDate, now() ) & hourSuffix;
		}
		if ( year( now() ) != year( parsedDate ) ) {
			dateMask = "d mmm, yyyy";
		}

		return lsDateFormat( parsedDate, dateMask );
	}

// PRIVATE METHODS
	private string function _renderTweet(
		  required struct  tweetData
		,          string  renderedBody  = ""
		,          string  renderedMedia = ""
		,          numeric replyCount    = 0
		,          numeric retweetCount  = 0
		,          numeric likeCount     = 0
		,          boolean isQuoted      = false
		,          string  context       = "default"
	) {
		var renderedBody   = len( arguments.renderedBody  ) ? arguments.renderedBody  : renderTweetBody(  arguments.tweetData, arguments.context, arguments.isQuoted );
		var renderedMedia  = len( arguments.renderedMedia ) ? arguments.renderedMedia : renderTweetMedia( arguments.tweetData, arguments.context );
		var renderedHeader = $renderContent( renderer="tweet_header", data=arguments, context=arguments.context );
		var renderedFooter = arguments.isQuoted ? "" : $renderContent( renderer="tweet_footer", data=arguments, context=arguments.context );
		var renderedQuoted = "";


		var isRetweet      = !isEmpty( arguments.tweetData.retweeted_status ?: {} );
		var quotedStatus   = isRetweet ? ( arguments.tweetData.retweeted_status.quoted_status ?: {} ) : ( arguments.tweetData.quoted_status ?: {} );
		if ( !arguments.isQuoted && !isEmpty( quotedStatus ) ) {
			renderedQuoted = _renderTweet(
				  tweetData = quotedStatus
				, isQuoted  = true
				, context   = arguments.context
			);
		}

		return trim( $renderContent(
			  renderer = "tweet"
			, data     = {
				  tweetData      = arguments.tweetData
				, renderedBody   = renderedBody
				, renderedHeader = renderedHeader
				, renderedFooter = renderedFooter
				, renderedMedia  = renderedMedia
				, renderedQuoted = renderedQuoted
				, isQuoted       = arguments.isQuoted
			  }
			, context  = arguments.context
		) );
	}

	private query function _getTweetById( required string id ) {
		return $getPresideObject( "twitter_status" ).selectData( id=arguments.id );
	}

	private query function _selectTweets() {
		return $getPresideObject( "twitter_status" ).selectData( argumentCollection=arguments );
	}

	private string function _getStringToReplace( required struct entity ) {
		var data = arguments.entity.data;

		switch( arguments.entity.type ) {
			case "hashtags":
				return "##" & data.text;
			case "symbols":
				return "$" & data.text;
			case "user_mentions":
				return "@" & data.screen_name;
			case "urls":
			case "media":
				return data.url;
		}
	}

	private numeric function _getInReplyToCount( required struct tweet ) {
		var displayStart = arguments.tweet.display_text_range[ 1 ] ?: 0;

		if ( !displayStart ) {
			return 0;
		}

		var inReplyTo    = left( arguments.tweet.full_text ?: "", displayStart );

		return listLen( inReplyTo, " @" );
	}

	private boolean function _useDefaultContext( required string renderer, required string context ) {
		return arguments.context == "default" || !$getColdbox().viewletExists( "renderers.content.#arguments.renderer#.#arguments.context#" );
	}

	private string function _renderEntity( required struct entity, string context="default" ) {
		var type     = arguments.entity.type;
		var renderer = "tweet_#type#";

		if ( type == "media" ) {
			renderer &= "_#arguments.entity.data.type#";
		}

		return trim( $renderContent( renderer=renderer, data=arguments.entity.data, context=arguments.context ) );
	}

	private array function _combineAndSortEntities( required struct tweet ) {
		var entities    = [];
		var allEntities = arguments.tweet.entities ?: {};

		for( var type in allEntities ) {
			if ( type == "polls" ) {
				continue;
			}
			for( var entity in allEntities[ type ] ) {
				entities.append( { type=type, data=entity } );
			}
		}

		entities.sort( function ( entity1, entity2 ) {
			var start1 = entity1.data.indices[ 1 ];
			var start2 = entity2.data.indices[ 1 ];

			if ( start1 > start2 ) {
				return 1;
			} else if ( start1 < start2 ) {
				return -1;
			} else {
				return 0;
			}
		} );

		return entities;
	}

	private array function _combineMediaEntities( required struct tweet ) {
		var entities      = [];
		var mediaEntities = arguments.tweet.extended_entities.media ?: [];
		var itemUrl       = "";
		var itemExtension = "";
		var itemBaseUrl   = "";
		var tweetId       = arguments.tweet.id_str ?: "";
		var uuid          = createUUID();

		mediaEntities.each( function( entity, index, array ) {
			itemUrl       = entity.media_url_https ?: "";
			itemExtension = listLast( itemUrl, "." );
			itemBaseUrl   = rereplace( itemUrl, "\.#itemExtension#$", "" );

			entity.itemData = {
				  tweetId   = tweetId
				, uuid      = uuid
				, count     = array.len()
				, index     = index
				, url       = itemUrl
				, baseUrl   = itemBaseUrl
				, extension = itemExtension
			};

			entities.append( { type="media", data=entity } );
		} );

		return entities;
	}

}