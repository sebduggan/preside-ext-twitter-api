component extends="preside.system.base.AdminHandler" {

	property name="apiWrapper" inject="TwitterApiWrapper";
	property name="messageBox" inject="coldbox:plugin:messageBox";

	public void function requestToken( event, rc, prc ) {
		var accountId    = rc.accountId ?: "";
		var requestToken = apiWrapper.getRequestToken( accountId );

		if ( isTrue( requestToken.getSuccess() ?: "" ) && isTrue( requestToken.getResponse().oauth_callback_confirmed ?: "" ) ) {
			var oauthToken = requestToken.getResponse().oauth_token ?: "";

			setNextEvent( url="https://api.twitter.com/oauth/authorize", querystring="oauth_token=#oauthToken#&force_login=true" );
		}

		dump( requestToken );
		abort;
	}

	public void function callback( event, rc, prc ) {
		var accountId     = rc.accountId      ?: "";
		var oAuthToken    = rc.oauth_token    ?: "";
		var oAuthVerifier = rc.oauth_verifier ?: "";
		var accessToken   = apiWrapper.getAccessToken( accountId=accountId, oAuthToken=oAuthToken, oAuthVerifier=oAuthVerifier );

		if ( !accessToken.getSuccess() ) {
			messageBox.error( translateResource( uri="twitter:oauth.link.account.error", data=[ accessToken.getResponse() ]) );
		}

		setNextEvent( url=event.buildAdminLink( objectName="twitter_account", recordId=accountId ) );
	}

}